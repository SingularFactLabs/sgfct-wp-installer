
# LOG

## 08-10-2017
  - If you wabt you can set  in apache as TLD : See https://blog.gaya.ninja/articles/faking-a-top-level-domain-name-for-local-development-with-apache/
  - See this as reference: https://css-tricks.com/intro-bedrock-wordpress/

  - Modify little script to install composer.php
  - Now you can run composer running php bin/composer
  - Add bedrock setup support. See : https://roots.io/bedrock/docs/installing-bedrock/

  - I create a local DB with mamp to test the scripts. 
  DB with collate (utf8_general_ci) pti-cms
  - Create user pti-csm with all permissions on the db pti-cms , with access only from localhost. password pti-cms

  - Change bedrock env file with these settings.
  - Sets some test local dir
  WP_HOME=http://localhost:12700

  - Installing plugins
    * pods
    * wpml : see : https://wpml.org/forums/topic/installing-wpml-via-composer/

  - Installing wp cli as a require lib in the bedrock composer.json  
  - We use wp-cli to install wp with this configuration
  ../bin//wp core install --url='http://localhost:12700' --title='pti-cms' --admin_user='admin' --admin_password='enter' --admin_email='cuevash.sp@gmail.com'

  - Activating the plugins
../bin//wp plugin activate wpml-multilingual-cms
../bin//wp plugin activate wpml-multilingual-cms
 


  