
# PROJECT

This project is a installer of Wordpress through the composer tool. It will create a minimal wordpress installation to be used as CMS.
It configures wodpress with a few plugins to have amultilingual wp with support for custom posts.
The plugins installed to achieve this are:
  * WPML (You need a licence for this plugin to run)
  * Pods
  *..

## Install instructions
Just run the installer ```ìnstall.sh```
You can do it in a interactive way, the program will ask for the details nedeed, or you can pass variables for a full automated installation way.

Once installed you can:

Run composer by:
``` php bin/composer ````

Now you install the bedrock wp template, see instructions: https://roots.io/bedrock/docs/installing-bedrock/

``` php bin/composer create-project roots/bedrock ```

The bedrock dir is created with a full up to date installation of wp, it needs to be configured set up the DB, SITE, etc configuration options fot the wp deployment. see instructions on the https://roots.io/bedrock/docs/installing-bedrock/ site.

You need to create the DB for wp before setting the next steps.





