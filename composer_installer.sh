php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (trim(hash_file('SHA384', 'composer-setup.php')) ===  trim(file_get_contents('https://composer.github.io/installer.sig', true)) ) 
  { echo 'Installer verified'; } 
  else 
  { echo 'Installer corrupt'; unlink('composer-setup.php'); };
  echo PHP_EOL;"
mkdir ./bin  
php composer-setup.php --install-dir=bin --filename=composer
php -r "unlink('composer-setup.php');"

# Installing wp with bedrock
php bin/composer create-project roots/bedrock

#Copying files
cp ./config/.env ./bedrock/.env

#updating from bedrock directory
php ../bin/composer update




